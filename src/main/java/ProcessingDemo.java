import processing.core.PApplet;

import java.util.Arrays;
import java.util.List;

class Ball{
    public static final int DIAMETER = 20;
    private int x;
    private int y;
    private int speed;

    public Ball(int x, int y, int speed){
        this.x = x;
        this.y = y;
        this.speed = speed;
    }
    public void move(){
        x+=speed;
    }
    public void draw(PApplet pApplet){
        pApplet.ellipse(x,y, DIAMETER,DIAMETER);
    }
}


public class ProcessingDemo extends PApplet {

    public static final int WIDTH = 700 ;
    public static final int HEIGHT = 640;
    public static final int DIAMETER = 20;
    public static final int SPEED1 = 1;
    public static final int SPEED2 = 2;
    public static final int SPEED3 = 3;
    public static final int SPEED4 = 4;

    Ball ball1;
    Ball ball2;
    Ball ball3;
    Ball ball4;
    List<Ball> balls;

    public static void main(String[] args) {
        PApplet.main("ProcessingDemo",args);
    }

    @Override
    public void settings() {
        size(700,640);
    }

    @Override
    public void setup() {
        ball1=new Ball(0,HEIGHT*1/4, SPEED1);
        ball2=new Ball(0,HEIGHT*2/4, SPEED2);
        ball3=new Ball(0,HEIGHT*3/4, SPEED3);
        ball4=new Ball(0,HEIGHT*4/4, SPEED4);
        balls = Arrays.asList(ball1, ball2, ball3, ball4);
    }

    @Override
    public void draw() {
        balls.forEach(Ball::move);
        balls.forEach(ball -> ball.draw(this));
    }
}
